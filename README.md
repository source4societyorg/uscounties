# Counties

Formats of US county data in SQL format (and CSV) with county name and state.

Source: https://en.wikipedia.org/wiki/List_of_United_States_counties_and_county_equivalents
